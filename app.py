import boto3
from flask import Flask, redirect, request, jsonify

app = Flask(__name__)

@app.route('/invoices/<docid>', methods=['GET'])
def invoices(docid):
    try:
        dynamodb = boto3.resource('dynamodb')

        print("###### docid:"+docid)

        if docid:

            table = dynamodb.Table('documents')

            response = table.get_item(
            Key={
                    'docid': docid
                }
            )

            try:
                item = response['Item']
            except (KeyError) as e:
                print('#### key error: ' + str(e))
                return invalid_request()

            print(item)

            return redirect(item['cfiles_url'])

        else:
            return invalid_request()

    except BaseException as e:
        print('#### Failed to do something: ' + str(e))
        return invalid_request()

def invalid_request():
    response = jsonify(dict(error='invalid request'))
    response.status_code = 400
    return response

if (__name__ == "__main__"): 
     app.run(debug=True)

