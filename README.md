## Overview
AWS Lambda serverless API example using AWS, Python and Zappa framework.
The app is a imaginary use case that will serve invoice documents to the customers using Lambda, API Gateway and DynamoDB.


### Requirements
* AWS Account
* AWS configured locally (~/.aws)
* AWS role with policy for lambda & dynamodb
* Python 3.3 and up
* Boto3
* Flask
* Zappa framework (to easily deploy and setup API gateway)
* Virtualenv (for zappa)
* $ git clone git@gitlab.com:nagjv/invoices-serv.git


### Local testing

```python

$ pip install -r requirements.txt
$ python app.py

```

### Zappa for AWS deployment
```python
$ virtualenv env
$ source env/bin/activate
$ pip install zappa
$ pip install -r requirements.txt
$ zappa deploy test
$ zappa update (if you have to update lambda)
```
